package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath    string
	FscConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	viper.AutomaticEnv()
	flags := serveCommand.Flags()

	flags.StringVarP(&serveOpts.ConfigPath, "mk-config-path", "", "./config/config.yaml", "Location of the config")
	flags.StringVarP(&serveOpts.FscConfigPath, "mk-fsc-config-path", "", "", "Location of the FSC config")
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New("../config/global.yaml", serveOpts.ConfigPath, serveOpts.FscConfigPath)
		if err != nil {
			slog.Error("config new failed", "err", err)
			return
		}
		var logLevel slog.Level
		if cfg.Debug {
			logLevel = slog.LevelDebug
		} else {
			logLevel = slog.LevelInfo
		}
		logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
			Level: logLevel,
		})).With("application", "http_server")
		logger.Info("Starting fictief loket backend", "config", cfg, "loglevel", logLevel)

		app := application.New(logger, cfg)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			logger.Error("listen and serve failed", "err", err)
			return
		}

		os.Exit(0)
	},
}
