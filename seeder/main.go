package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"net/http"
	"net/http/httputil"
	"os"
	"time"

	"golang.org/x/exp/slog"

	randomdata "github.com/Pallinder/go-randomdata"
	"github.com/google/uuid"
	"github.com/oapi-codegen/runtime/types"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

type sliceStrFlag []string

func (i *sliceStrFlag) String() string {
	return "my string representation"
}

func (i *sliceStrFlag) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func main() {
	bvvServer := flag.String("bvv-server", "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0", "bvv endpoint to send seed data to")
	bvvSeedCount := flag.Int("bvv-seed-count", 100, "amount of vreemdelingen to create")
	observationSeedCount := flag.Int("observation-seed-count", 5, "maximum amount of attributes to create per vreemdeling")
	processSeedCount := flag.Int("sigma-process-seed-count", 5, "maximum amount of process to create per vreemdeling")
	processAttributeSeedCount := flag.Int("sigma-process-attribute-seed-count", 5, "maximum amount of attributes create per process")

	var sigmaServers sliceStrFlag
	flag.Var(&sigmaServers, "sigma-servers", "sigma endpoint to send seed data to")
	flag.Parse()

	var programLevel = new(slog.LevelVar)
	programLevel.Set(slog.LevelWarn)
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: programLevel}))
	slog.SetDefault(logger)

	cfg, err := NewConfig("./config/global.yaml")
	if err != nil {
		panic(err)
	}

	bvvClient, err := bvv.NewClient(*bvvServer)
	if err != nil {
		panic(err)
	}

	sigmaClients := []*sigma.Client{}
	for _, server := range sigmaServers {
		sigmaClient, err := sigma.NewClient(server)
		if err != nil {
			panic(err)
		}

		sigmaClients = append(sigmaClients, sigmaClient)
	}

	for i := 1; i <= *bvvSeedCount; i++ {
		if err := func() error {
			id, err := createVreemdeling(bvvClient)
			if err != nil {
				return fmt.Errorf("vreemdeling creation failed: %w", err)
			}

			for j := 0; j <= randomdata.Number(*observationSeedCount); j++ {
				if err := createAttribute(sigmaClients, cfg, id); err != nil {
					return fmt.Errorf("attribute creation failed: %w", err)
				}
			}

			for j := 0; j <= randomdata.Number(*processSeedCount); j++ {
				client := sigmaClients[randomdata.Number(len(sigmaClients))]

				if err := createProcess(client, cfg, id, *processAttributeSeedCount); err != nil {
					return fmt.Errorf("proces creation failed: %w", err)
				}
			}

			return nil
		}(); err != nil {
			slog.Error("Seed round failed", "err", err)
		}
	}
}

func createVreemdeling(client *bvv.Client) (string, error) {
	date := randomdata.FullDateInRange("1920-01-01", "2023-12-31")
	birthDate, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return "", err
	}

	body := bvv.CreateVreemdelingJSONBody{
		Data: bvv.VreemdelingWithoutId{
			Geboortedatum: &types.Date{
				Time: birthDate,
			},
			Naam: randomdata.FullName(randomdata.RandomGender),
		},
	}

	resp, err := client.CreateVreemdeling(context.Background(), nil, bvv.CreateVreemdelingJSONRequestBody(body))
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 201 {
		return "", fmt.Errorf("failed to create vreemdeling %v %v", resp.StatusCode, resp.Status)
	}

	var model bvv.VreemdelingResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return "", err
	}

	slog.Info("Created vreemdeling", "data", model)

	return model.Data.Id, nil
}

func createAttribute(clients []*sigma.Client, cfg *Config, id string) error {
	client := clients[randomdata.Number(len(clients))]

	rubriek := getRandRubriek(cfg)
	attribute := getRandAttribute(rubriek.Attributes)
	data, err := getRandAttributeData(attribute)
	if err != nil {
		return err
	}

	vwlID := uuid.New()
	systeem := "sigma"
	activiteit := "https://example.com/activity/v0/observation-create"
	verantwoordelijke := "seeder"

	params := &sigma.CreateObservationByVreemdelingenIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &systeem,
		FSCVWLVerwerker:             &verantwoordelijke,
		FSCVWLVerantwoordelijke:     &verantwoordelijke,
	}

	body := sigma.CreateObservationByVreemdelingenIdJSONRequestBody{
		Data: sigma.ObservationCreate{
			Attribute: attribute.Name,
			Value:     *data,
		},
	}

	resp, err := client.CreateObservationByVreemdelingenId(context.Background(), id, params, body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		r, _ := httputil.DumpResponse(resp, true)
		return fmt.Errorf("failed to create attribute %d %s\n%s\n%+v", resp.StatusCode, resp.Status, r, body)
	}

	var model sigma.ObservationCreateResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return err
	}

	slog.Info("Created observation", "data", model.Data)
	return nil
}

func createProcess(client *sigma.Client, cfg *Config, id string, attributeAmount int) error {
	proces := getRandProcess(cfg)
	procesStatus := getRandProcessStatus(&proces)

	amount := randomdata.Number(attributeAmount)

	attributes := make([]sigma.ObservationCreate, 0, amount)
	for i := 0; i < amount; i++ {
		attribute := getRandAttribute(proces.Attributes)
		data, err := getRandAttributeData(attribute)
		if err != nil {
			return err
		}

		attributes = append(attributes, sigma.ObservationCreate{
			Attribute: attribute.Name,
			Value:     *data,
		})
	}

	vwlID := uuid.New()
	systeem := "sigma"
	activiteit := "https://example.com/activity/v0/proces-create"
	verantwoordelijke := "seeder"

	params := &sigma.CreateProcesByVreemdelingenIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &systeem,
		FSCVWLVerwerker:             &verantwoordelijke,
		FSCVWLVerantwoordelijke:     &verantwoordelijke,
	}

	body := sigma.CreateProcesByVreemdelingenIdJSONRequestBody{
		Data: sigma.ProcesCreate{
			Attributes: attributes,
			Status:     procesStatus.Name,
			Type:       proces.Name,
		},
	}

	resp, err := client.CreateProcesByVreemdelingenId(context.Background(), id, params, body)
	if err != nil {
		return fmt.Errorf("create proces by vreemdelingen id failed: %w", err)
	}

	if resp.StatusCode != http.StatusCreated {
		r, _ := httputil.DumpResponse(resp, true)
		slog.Error("failed to create attribute", "status code", resp.StatusCode, "status", resp.Status, "response", string(r), "body", body)
		return fmt.Errorf("failed to create attribute %d %s\n%s\n%+v", resp.StatusCode, resp.Status, r, body)
	}

	var model sigma.ProcesCreateResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return fmt.Errorf("proces create response decode failed: %w", err)
	}

	slog.Info("Created proces", "data", model.Data)
	return nil
}

func getRandProcess(cfg *Config) Proces {
	return cfg.Processen[randomdata.Number(len(cfg.Processen))]
}

func getRandProcessStatus(proces *Proces) ProcesStatus {
	return proces.Statuses[randomdata.Number(len(proces.Statuses))]
}

func getRandRubriek(cfg *Config) Rubriek {
	return cfg.Rubrieken[randomdata.Number(len(cfg.Rubrieken))]
}

func getRandAttribute(attributes []Attribute) Attribute {
	return attributes[randomdata.Number(len(attributes))]
}

func getRandAttributeData(attribute Attribute) (*sigma.Value, error) {
	var err error
	var a sigma.Value

	switch attribute.Type {
	case "text":
		if attribute.Name == "alias" {
			err = a.FromValue1(randomdata.SillyName())
		} else {
			err = a.FromValue1(randomdata.Paragraph())
		}
	case "checkboxes":
		a, err = getRandAttributeDataMultiple(attribute)
	case "datetime":
		a, err = getRandAttributeDataTimedate(attribute)
	case "date":
		a, err = getRandAttributeDataDate(attribute)
	case "number":
		a, err = getRandAttributeNumber(attribute)
	case "boolean":
		err = a.FromValue2(randomdata.Boolean())
	case "textarea":
		err = a.FromValue1(randomdata.Paragraph())
	case "radios":
		a, err = getRandAttributeDataSingle(attribute)
	case "select":
		if attribute.Multiple {
			a, err = getRandAttributeDataMultiple(attribute)
		} else {
			a, err = getRandAttributeDataSingle(attribute)
		}
	default:
		return nil, fmt.Errorf("attribute type unknown: %sname:%s%+v", attribute.Type, attribute.Label, attribute)
	}

	return &a, err
}

func getRandAttributeDataSingle(attribute Attribute) (sigma.Value, error) {
	index := randomdata.Number(len(attribute.Options))

	value := attribute.Options[index].Name

	var a sigma.Value
	if err := a.FromValue1(value); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataMultiple(attribute Attribute) (sigma.Value, error) {
	amount := randomdata.Number(int(math.Min(float64(len(attribute.Options)), 5)))

	options := attribute.Options
	value := make([]interface{}, 0, amount)

	for i := 0; i <= int(amount); i++ {
		index := randomdata.Number(len(options))
		option := options[index]
		value = append(value, option.Name)

		options = remove(options, index)
	}

	var a sigma.Value
	err := a.FromValue3(value)

	return a, err
}

func getRandAttributeNumber(attribute Attribute) (sigma.Value, error) {
	min := math.MinInt / 2
	max := math.MaxInt / 2

	if attribute.Min != nil {
		min = (*attribute.Min).(int)
	}

	if attribute.Max != nil {
		max = (*attribute.Max).(int)
	}

	var a sigma.Value
	if err := a.FromValue4(float32(randomdata.Number(min, max))); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataTimedate(_ Attribute) (sigma.Value, error) {
	var a sigma.Value
	date := randomdata.FullDate()
	d, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return a, err
	}

	if err := a.FromValue1(d.Format(time.RFC3339)); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataDate(attribute Attribute) (sigma.Value, error) {
	date := randomdata.FullDate()
	if attribute.Name == "familieGeboortedatum" {
		date = randomdata.FullDateInRange("1920-01-01", time.Now().Format("2006-01-02"))
	}

	var a sigma.Value
	d, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return a, err
	}

	if err := a.FromValue1(d.Format("2006-01-02")); err != nil {
		return a, err
	}

	return a, nil
}

func remove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
