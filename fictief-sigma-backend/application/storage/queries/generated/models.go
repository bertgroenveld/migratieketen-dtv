// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0

package queries

import (
	"database/sql"
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type SigmaObservation struct {
	ID            uuid.UUID
	VreemdelingID string
	Attribute     string
	Value         json.RawMessage
	CreatedAt     time.Time
	DeletedAt     sql.NullTime
}

type SigmaProcess struct {
	ID            uuid.UUID
	ProcessID     string
	VreemdelingID string
	Type          string
	Status        string
	CreatedAt     time.Time
	DeletedAt     sql.NullTime
}

type SigmaProcessAttribute struct {
	ID        uuid.UUID
	ProcessID uuid.UUID
	Attribute string
	Value     json.RawMessage
	CreatedAt time.Time
	DeletedAt sql.NullTime
}
