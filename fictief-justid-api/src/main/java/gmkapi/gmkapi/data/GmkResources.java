package gmkapi.gmkapi.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class GmkResources {
    private String uniekeIdentifier;
    private String naam;
    private String alternatieveAanduiding;
    private String definitie;
    private String status;
    private List<GmkBronnen> bronnen;
}
