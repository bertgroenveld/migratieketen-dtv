# available variables:
# - $vreemdeling_id - id of newly created vreemdeling. Do not delete this vreemdeling, because other tests may depend on it.

echo 'Add observations for attribute with "min" aggregator to SIGMA:'
resp=$(echo '{
    "data": {
        "attribute": "minderjarigeKinderen",
        "value": 1
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id/observations" \
)
printf '%s' "$resp" | jq .
resp=$(echo '{
    "data": {
        "attribute": "minderjarigeKinderen",
        "value": 2
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id/observations" \
)
printf '%s' "$resp" | jq .

echo 'Get attribute with "min" aggregator from loket:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://shared-loket-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id?attributes=minderjarigeKinderen&sources=fictief-np" \
)
printf '%s' "$resp" | jq .
